import axios from 'axios'

const products = {
  namespaced: true,
  state: {
    products: []
  },
  mutations: {
    SET_PRODUCTS (state, data) {
      state.products = data
    }
  },
  actions: {
    loadProducts ({ commit }) {
      return new Promise((resolve, reject) => {
        axios
          .get('/data/products.json')
          .then(res => {
            const response = res.data.map(item => {
              return {
                id: item.productId,
                name: item.productName,
                description: item.metaTagDescription,
                brand: item.brand,
                images: item.items[0].images.map(image => image.imageUrl),
                availableQuantity: item.items[0].sellers[0].commertialOffer.AvailableQuantity,
                price: item.items[0].sellers[0].commertialOffer.Price,
                sizes: JSON.parse(item['Tabela de Medidas']).reduce((table, size) => Object.assign(size, table), {}),
                installments: item.items[0].sellers[0].commertialOffer.Installments.slice(-1)[0]
              }
            })

            commit('SET_PRODUCTS', response)
            resolve()
          })
          .catch(error => {
            console.log(error)
          })
      })
    }
  },
  getters: {
    getProducts: state => {
      return state.products
    }
  }
}

export default products
