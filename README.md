# Desafio FrontEnd
`Fabricio Anciães Pereira`

Projeto desenvolvido em vue.js utilizando vuex e sass.

### Instruções
1. Clone o repositório
2. rode `npm install`
3. rode `npm run serve` para abrir o ambiente de desenvolvimento

A parte mobile é a que está mais completa, recomendo que simule um device mobile usando o devtools:

![Devtools mobile](/readme/devtools.png)

### Progresso

- ✔️Interação com JSON para renderizar os produtos
- ❌Filtro de produtos funcional
- ❌Adicionar produtos ao carrinho
- ✔️Botão de carregar mais produtos
- ✔️Layout Mobile
- ❌Layout Desktop

#### Home
![Home](/readme/home.gif)

#### Produtos
![Home](/readme/products.gif)


#### Considerações Técnicas

Carreguei o json dos produtos diretamente do arquivo, numa situação real com uma aplicação em produção eu teria usado uma api pra isso, incluindo os filtros/paginação todos vindos diretamente da api. Para simplificar, acabei fazendo por arquivo mesmo e usando javascript para extrair os dados que me interessavam.

A checagem de device mobile usa user agents, somente diminuir a tela não funcionará.

Gostaria de ter feito completamente responsivo mas não sei se isso ia desviar muito da proposta original do teste.

Infelizmente como tive que dividir o tempo com o meu emprego atual, só consegui trabalhar aos fins de semana. Gostaria de ter feito bem mais.

Alguns itens nos arquivos de layout do Adobe XD eram recortes de screenshots, por tanto não consegui replicar com muita precisão, pois não tinha acesso às medidas. Na impossibilidade de pegar medidas dos arquivos de layout eu recorri ao site da animale, inclusive pra ícones e fontes.

O sass poderia ter sido melhor organizado, fazendo um uso mais inteligente de mixins e funções. Existe código repetido (declarações de transform, cores, etc). Uma forma de mitigar parte disso é, durante o processo de design, projetar primeiro blocos reutilizáveis, como um UI Kit. Assim facilita bastante a visão do dev sobre como componentizar a aplicação.

Muito obrigado pela oportunidade, espero que gostem do meu trabalho.